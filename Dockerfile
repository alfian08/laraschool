FROM php:7.4-apache
RUN apt-get update && \
    apt-get install -y \
        libzip-dev \
        zip \
        unzip \
        && docker-php-ext-install zip pdo pdo_mysql mysqli
WORKDIR /var/www/html
COPY . /var/www/html
RUN chmod +x /var/www/html/runserver.sh
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ARG COMPOSER_ALLOW_SUPERUSER=1
RUN composer install
RUN chown -R www-data:www-data /var/www/html
COPY .env.example .env
RUN sed -i 's/DB_HOST=.*/DB_HOST=172.17.0.1/' /var/www/html/.env
RUN sed -i 's/DB_PORT=.*/DB_PORT=3306/' /var/www/html/.env
RUN sed -i 's/DB_DATABASE.*/DB_DATABASE=laraschool/' /var/www/html/.env
RUN sed -i 's/DB_USERNAME.*/DB_USERNAME=alfian/' /var/www/html/.env
RUN sed -i 's/DB_PASSWORD.*/DB_PASSWORD=tirta/' /var/www/html/.env
RUN php artisan optimize:clear
RUN php artisan key:generate
EXPOSE 8080
ENTRYPOINT /var/www/html/runserver.sh
